<!DOCTYPE html>
<html>
	<head>
		<title>Redirigiendo...</title>
		<meta http-equiv="refresh" content="0;url=http://www.redsemillaslibres.co/wp-content/404.html">
	</head>
	<body>
		<script type="text/javascript">
			window.location = "http://www.redsemillaslibres.co/wp-content/404.html";
		</script>

		<p>Estás siendo redirigido a <a href="http://www.redsemillaslibres.co/wp-content/404.html">http://www.redsemillaslibres.co/wp-content/404.html</a></p>
	</body>
</html>
